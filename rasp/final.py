import Adafruit_DHT as dht
from time import sleep
import RPi.GPIO as GPIO
import time
import requests

SOUND_PIN = 17 # BCM mode.
TEMP_AND_HUMADITY_PIN = 4 # BCM mode.
MQ_PIN = 18
POOP_HUMADITY_THRESHOLD = 50
SERVER_ADDRESS = '82.115.21.9:8181'

GPIO.setmode(GPIO.BCM)
GPIO.setup(SOUND_PIN, GPIO.IN)
GPIO.setup(MQ_PIN, GPIO.IN)

humadity = None
temperature = None
has_pooped = 0
gas_leak = 0
noise = 0

while True:
    humadity,temperature = dht.read_retry(dht.DHT22, TEMP_AND_HUMADITY_PIN)
    gas_leak = not GPIO.input(MQ_PIN)
    noise = GPIO.input(SOUND_PIN)
    
    if humadity > POOP_HUMADITY_THRESHOLD:
        has_pooped = 1

    time.sleep(0.5) 


    temperature_message = 'Temperate={0:0.1f}*C'.format(temperature)
    print(temperature_message)
    print('Humidity={0:0.1f}%'.format(humadity))
    print("Gas leak: ", gas_leak)
    print("Sound pollution: ", noise)
    print('***********************************************************')

    if gas_leak:
        requests.get(f"http://{SERVER_ADDRESS}/gahvareh/event/?event=urinate")

    if has_pooped:
        requests.get(f"http://{SERVER_ADDRESS}/gahvareh/event/?event=poop")

    if noise:
        requests.get(f"http://{SERVER_ADDRESS}/gahvareh/event/?event=cry")

    requests.get(f"http://{SERVER_ADDRESS}/gahvareh/event/?event=Temperature&information={int(temperature)}")






