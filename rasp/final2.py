import Adafruit_DHT as dht
from time import sleep
import RPi.GPIO as GPIO
import time
import requests
import threading

SOUND_PIN = 17 # BCM mode.
TEMP_AND_HUMADITY_PIN = 4 # BCM mode.
MQ_PIN = 18
POOP_HUMADITY_THRESHOLD = 50
SERVER_ADDRESS = '82.115.21.9:8181'

GPIO.setmode(GPIO.BCM)
GPIO.setup(SOUND_PIN, GPIO.IN)
GPIO.setup(MQ_PIN, GPIO.IN)

humadity = None
temperature = None
has_pooped = 0
gas_leak = 0
noise = 0

def handle_am2302():
    try:
        has_pooped = False
        while True:
            humadity,temperature = dht.read_retry(dht.DHT22, TEMP_AND_HUMADITY_PIN)
            if humadity > POOP_HUMADITY_THRESHOLD:
                has_pooped = 1

            temperature_message = 'Temperate={0:0.1f}*C'.format(temperature)
            print(str.center(" start am2302 ", 50, '*'))
            print(temperature_message)
            print('Humidity={0:0.1f}%'.format(humadity))
            print(str.center(" end am2302 ", 50, '*'))

            if has_pooped:
                requests.get(f"http://{SERVER_ADDRESS}/gahvareh/event/?event=poop")
            requests.get(f"http://{SERVER_ADDRESS}/gahvareh/event/?event=Temperature&information={int(temperature)}")

            time.sleep(5) 
    except KeyboardInterrupt:
        return
    except:
        pass
        
def handle_ky037():
    try:
        while True:
            noise = GPIO.input(SOUND_PIN)
            print("Sound pollution: ", noise)

            if noise:
                requests.get(f"http://{SERVER_ADDRESS}/gahvareh/event/?event=cry")

            time.sleep(0.5) 
    except KeyboardInterrupt:
        return
    except:
        pass
  
def handle_mq5():
    try:
        while True:
            gas_leak = not GPIO.input(MQ_PIN)
            print("Gas leak: ", gas_leak)

            if gas_leak:
                requests.get(f"http://{SERVER_ADDRESS}/gahvareh/event/?event=urinate")

            time.sleep(0.5) 
    except KeyboardInterrupt:
        return
    except:
        pass

try:
    t1 = threading.Thread(target=handle_am2302)
    t2 = threading.Thread(target=handle_ky037)
    t3 = threading.Thread(target=handle_mq5)

    t1.start()
    t2.start()
    t3.start()

    t1.join()
    t2.join()
    t3.join()
except:
    print("Error: unable to start app")
    