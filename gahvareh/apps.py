from django.apps import AppConfig


class GahvarehConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'gahvareh'
