from django.urls import path, include
from .views import EventsView, StatusApiView, status_view

urlpatterns = [
    path('event/', EventsView.as_view()),
    # path('status/', StatusApiView.as_view()),
    path('status/', status_view, name='status'),
]