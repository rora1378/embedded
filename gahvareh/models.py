from django.db import models, transaction

# Create your models here.


class EventTypes(models.IntegerChoices):
    Urinate = 0
    Poop = 1
    Cry = 2
    Temperature = 3
    Reset = 4


class BaseModel(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Status(BaseModel):
    urinated = models.BooleanField(default=False)
    pooped = models.BooleanField(default=False)
    crying = models.BooleanField(default=False)
    temperature = models.IntegerField(default=25)

    def baby_urinated(self):
        self.urinated = True
        self.save(update_fields=['urinated', 'updated'])

    def baby_pooped(self):
        self.pooped = True
        self.save(update_fields=['pooped', 'updated'])

    def baby_crying(self):
        self.crying = True
        self.save(update_fields=['crying', 'updated'])

    def baby_temperature(self, temperature):
        self.temperature = temperature
        self.save(update_fields=['temperature', 'updated'])

    def baby_reset(self):
        self.urinated = False
        self.pooped = False
        self.crying = False
        self.temperature = 25
        self.save()


class Event(BaseModel):
    event_type = models.PositiveSmallIntegerField(choices=EventTypes.choices)
    information = models.CharField(max_length=255, null=True, blank=True)

    def clean(self):
        if self.event_type == EventTypes.Temperature.value:
            if self.information is None or not self.information.isnumeric():
                raise Exception('مقدار دما درست مشخص نشده است.')

    def save(self, *args, **kwargs):
        if not self.pk:
            self.clean()
            super(Event, self).save(*args, **kwargs)
            self.update_status()
        else:
            super(Event, self).save(*args, **kwargs)

    @classmethod
    def get_status_obj_with_lock(cls):
        status_obj = Status.objects.last()
        if status_obj is None:
            status_obj = Status.objects.create()
        return Status.objects.select_for_update().get(id=status_obj.id)

    def update_status(self):
        with transaction.atomic():
            status_obj: Status = Event.get_status_obj_with_lock()
            if self.event_type == EventTypes.Urinate.value:
                status_obj.baby_urinated()
            elif self.event_type == EventTypes.Poop.value:
                status_obj.baby_pooped()
            elif self.event_type == EventTypes.Cry.value:
                status_obj.baby_crying()
            elif self.event_type == EventTypes.Temperature.value:
                status_obj.baby_temperature(temperature=int(self.information))
            elif self.event_type == EventTypes.Reset.value:
                status_obj.baby_reset()


