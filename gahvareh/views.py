from datetime import datetime

from django.http import Http404
from django.shortcuts import render

# Create your views here.
from rest_framework import status, generics
from rest_framework.response import Response
from rest_framework.views import APIView
from .models import Event, Status, EventTypes
from .serializers import StatusSerializer


class EventsView(APIView):
    @staticmethod
    def get(request):
        try:
            event_type = request.GET.get('event', None)
            information = request.GET.get('information', None)
            if event_type is None:
                return Response({'error': 'نوع رویداد مشخص نشده است.'}, status=status.HTTP_400_BAD_REQUEST)

            event_type = str(event_type).capitalize()
            if event_type not in EventTypes.names:
                return Response({'error': 'نوع رویداد تعریف نشده است.'}, status=status.HTTP_400_BAD_REQUEST)

            Event.objects.create(event_type=EventTypes[str(event_type)], information=information)
            return Response({'message': 'رویداد با موفقیت ثبت شد'}, status=status.HTTP_200_OK)
        except Exception as ve:
            return Response({'error': str(ve)}, status=status.HTTP_400_BAD_REQUEST)


def status_view(request):
    context = {}
    if request.method == 'GET':
        try:
            last_status = Status.objects.last()
            assert isinstance(last_status, Status)
            context['status'] = StatusSerializer(last_status).data
            context['last_updated_minutes'] = datetime.now().minute - last_status.updated.minute
        except Status.DoesNotExist:
            raise Http404("friend not found")
        return render(request, 'gahvareh/status.html', context)


class StatusApiView(generics.RetrieveAPIView):
    serializer_class = StatusSerializer

    def get_object(self):
        return Status.objects.last()
